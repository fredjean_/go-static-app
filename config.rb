set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  activate :gzip

  # Or use a different image path
  # set :http_path, "/Content/images/"
end

activate :directory_indexes

data.team.each do |member|
  proxy "/team/#{member.slug}.html", "team/team.html", locals: { member: member }
end

activate :s3_sync do |sync|
  sync.bucket = 'go-static.fredjean.net'
  sync.region = 'us-west-1'
end
